from django.contrib import admin
from .models import Device, MacAddress
# Register your models here.
admin.site.register(Device)
admin.site.register(MacAddress)
