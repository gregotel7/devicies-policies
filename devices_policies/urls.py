from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    # To have correct redirection when we try to access something when we are not logged in
    path('accounts/',include('django.contrib.auth.urls')),
    path('users/', include('django.contrib.auth.urls')),
    # Local apps
    path('users/', include('users.urls')),
    path('', include('pages.urls')),
    path('', include('devices.urls')),
    path('', include('policies.urls')),
]
