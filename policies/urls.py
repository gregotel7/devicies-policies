from django.urls import path
from . import views

app_name = 'policies'

urlpatterns = [
    path('policies/', views.PolicyListView.as_view(), name='policies'),
    path('policies/create/', views.PolicyCreateView.as_view(), name='policy_create'),
    path('policies/<int:pk>/', views.PolicyDetailView.as_view(), name='policy_detail'),
    path('policies/<int:pk>/update/', views.PolicyUpdateView.as_view(), name='policy_update'),
    path('policies/<int:pk>/delete/', views.PolicyDeleteView.as_view(), name='policy_delete'),
]