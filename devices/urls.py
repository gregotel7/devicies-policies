from django.urls import path
from . import views

app_name = 'devices'

urlpatterns = [
    path('devices/', views.DeviceListView.as_view(), name='devices'),
    path('devices/create/', views.DeviceCreateView.as_view(), name='device_create'),
    path('devices/<slug:slug>/', views.DeviceDetailView.as_view(), name='device_detail'),
    path('devices/<slug:slug>/update/', views.DeviceUpdateView.as_view(), name='device_update'),
    path('devices/<slug:slug>/delete/', views.DeviceDeleteView.as_view(), name='device_delete'),
    path('devices/<slug:slug>/macaddress_create/', views.MacAddressCreateView.as_view(), name='macaddress_create'),
    path('devices/<slug:slug>/macaddress_<int:pk>/update/', views.MacAddressUpdateView.as_view(), name='macaddress_update'),
    path('devices/<slug:slug>/macaddresses_<int:pk>/delete/', views.MacAddressDeleteView.as_view(), name='macaddress_delete'),
]