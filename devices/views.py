from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Device, MacAddress
from policies.models import Policy



class DeviceListView(LoginRequiredMixin, ListView):
    model = Device
    context_object_name = 'device_list'
    template_name = 'devices/device_list.html'
    
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Device.objects.all()
        else:
            return Device.objects.filter(registrator=self.request.user)


class DeviceDetailView(LoginRequiredMixin, DetailView):
    model = Device
    context_object_name = 'device'
    template_name = 'devices/device_detail.html'


class DeviceCreateView(LoginRequiredMixin, CreateView):
    model = Device
    template_name = 'devices/device_form.html'
    fields = ('manufacturer', 'name', 'website', 'policies',)
    
    def clean_name(self):
        return self.cleaned_date['name'].replace('','_')
    
    def form_valid(self, form):
        form.instance.registrator = self.request.user
        return super().form_valid(form)
    
    
class DeviceUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Device
    template_name = 'devices/device_form_update.html'
    fields = ('manufacturer', 'name', 'website', 'policies',)
    
    def test_func(self):
        obj = self.get_object()
        return obj.registrator == self.request.user or self.request.user.is_superuser
    
class DeviceDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Device
    template_name = 'devices/device_confirm_delete.html'
    success_url = reverse_lazy('devices:devices')
    
    def test_func(self):
        obj = self.get_object()
        return obj.registrator == self.request.user or self.request.user.is_superuser

    
    
class MacAddressCreateView(LoginRequiredMixin, CreateView):
    model = MacAddress
    template_name = 'devices/mac_address_form.html'
    fields = ('mac_address',)
    
    def get_context_data(self, **kwargs):
        context = super(MacAddressCreateView, self).get_context_data(**kwargs)
        context['slug'] = self.kwargs['slug']
        return context
    
    def form_valid(self, form):
        form.instance.registrator = self.request.user or self.request.user.is_superuser
        form.instance.device = Device.objects.get(slug=self.kwargs['slug'])
        return super().form_valid(form)
    
    
class MacAddressUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = MacAddress
    template_name = 'devices/mac_address_form_update.html' 
    fields = ('mac_address',)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['slug'] = self.kwargs['slug']
        context['pk'] = self.kwargs['pk']
        return context
    
    def test_func(self):
        obj = self.get_object()
        return obj.registrator == self.request.user or self.request.user.is_superuser
    
    
class MacAddressDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = MacAddress
    template_name = 'devices/mac_address_confirm_delete.html'
    success_url = reverse_lazy('devices:devices')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['slug'] = self.kwargs['slug']
        context['pk'] = self.kwargs['pk']
        return context
    
    def test_func(self):
        obj = self.get_object()
        return obj.registrator == self.request.user or self.request.user.is_superuser

    
