from django.conf import settings
from django.db import models
from django_extensions.db.fields import AutoSlugField
from django.urls import reverse


    
class Device(models.Model):
    registrator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    manufacturer = models.CharField(max_length=45, null=False, blank=False, verbose_name='Manufacturer', db_index=True)
    name = models.CharField(unique=True, max_length=45, null=False, blank=False, verbose_name='Model Name', db_index=True)
    slug = AutoSlugField(populate_from=['manufacturer', 'name'], unique=True)
    website = models.URLField(max_length=255, verbose_name='Website')
    policies = models.ManyToManyField('policies.Policy', related_name='devices', blank=True)
    
    class Meta:
        db_table = 'Device'
        ordering = ('manufacturer','name')
        
        
    def __str__(self):
        return "{}: {}".format(self.manufacturer, self.name)
    
    def get_absolute_url(self):
        return reverse('devices:device_detail', kwargs={'slug':self.slug})
    

class MacAddress(models.Model):
    registrator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    mac_address = models.CharField(unique=True, verbose_name='MAC Address', max_length=17, null=False, blank=False)
    device = models.ForeignKey('Device', on_delete=models.CASCADE, blank=True, related_name='macAddresses')

    class Meta:
        db_table = 'MacAddress'
        verbose_name_plural = 'Mac_Addresses'
        
    def __str__(self):
        return "{}: {}".format(self.mac_address, self.device.name)
    
    def get_absolute_url(self):
        return reverse('devices:device_detail', kwargs={'slug': self.device.slug})
    
    def get_update_url(self):
        return reverse('devices:macaddress_update', kwargs={'slug':self.device.slug, 'pk':self.id})
    
    def get_delete_url(self):
        return reverse('devices:macaddress_delete', kwargs={'slug':self.device.slug, 'pk': self.id})
    
    