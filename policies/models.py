from django.conf import settings
from django.db import models
from django_extensions.db.fields import AutoSlugField
from django.urls import reverse

class Policy(models.Model):
    registrator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    perm_dest_ports = models.CharField(max_length=255, blank=True, null=True, verbose_name='Permitted Destination Ports', help_text='Write the port numbers and seperate them using ",".')
    perm_dest_ips = models.CharField(max_length=512, blank=True, null=True, verbose_name='Permitted Destination IPs', help_text='Write the IP Addresses and seperate them using ",".')
    max_size_packet = models.PositiveIntegerField(verbose_name='Maximum Size Packet', null=True, blank=True)
    max_data_rate = models.PositiveIntegerField(verbose_name='Maximum Data Rate', null=True, blank=True)
    starting_time = models.TimeField(blank=True, null=True, help_text='Starting time of the policy.')
    ending_time = models.TimeField(blank=True, null=True, help_text='Ending time of the policy.' )
    restriction_level = models.CharField(max_length=1, null=False, blank=False, verbose_name='Restriction Level', help_text='Values from 1 to 5, with 1 for the least and 5 for the most restricted.')
    
    class Meta:
        db_table = 'Policy'
        verbose_name_plural = 'Policies'
    
    def __str__(self):
        return "Policy Number: {}".format(self.pk)
    
    def get_absolute_url(self):
        return reverse('policies:policy_detail', kwargs={'pk': self.pk})
    
    def get_update_url(self):
        return reverse('policies:policy_update', kwargs={'pk':self.pk})
    
    def get_delete_url(self):
        return reverse('policies:policy_delete', kwargs={'pk': self.pk})