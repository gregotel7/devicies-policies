from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Policy
from django.urls import reverse_lazy


class PolicyListView(LoginRequiredMixin, ListView):
    model = Policy
    context_object_name = 'policy_list'
    template_name = 'policies/policy_list.html'


class PolicyDetailView(LoginRequiredMixin, DetailView):
    model = Policy
    context_object_name = 'policy'
    template_name = 'policies/policy_detail.html'
    

class PolicyCreateView(LoginRequiredMixin, CreateView):
    model = Policy
    template_name = 'policies/policy_form.html'
    fields = ('perm_dest_ports', 'perm_dest_ips', 'max_size_packet', 'max_data_rate', 'starting_time', 'ending_time', 'restriction_level')
    
    def form_valid(self, form):
        form.instance.registrator = self.request.user
        return super().form_valid(form)
    
    
class PolicyUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Policy
    template_name = 'policies/policy_form_update.html'
    fields = ('perm_dest_ports', 'perm_dest_ips', 'max_size_packet', 'max_data_rate', 'starting_time', 'ending_time', 'restriction_level')

    def test_func(self):
        obj = self.get_object()
        return obj.registrator == self.request.user or self.request.user.is_superuser
    

class PolicyDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Policy
    template_name = 'policies/policy_confirm_delete.html'
    success_url = reverse_lazy('policies:policies')
    
    def test_func(self):
        obj = self.get_object()
        return obj.registrator == self.request.user or self.request.user.is_superuser