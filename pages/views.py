from django.views.generic import TemplateView


class HomePageView(TemplateView):
    template_name = 'pages/homepage.html'


class AboutPageView(TemplateView): 
    template_name = 'pages/about.html'
    
    
